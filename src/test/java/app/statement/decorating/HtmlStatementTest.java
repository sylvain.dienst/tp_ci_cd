package app.statement.decorating;

import app.Customer;
import app.Rental;
import app.movie.Movie;
import app.movie.movieclass.ChildrenMovie;
import app.movie.movieclass.RegularMovie;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class HtmlStatementTest {
    Customer c1;

    @Before
    public void setUp() throws Exception {
        this.c1 = new Customer("Sylvain");
        c1.addRentals(new Rental(new Movie(new ChildrenMovie("Film enfant")), 5));
        c1.addRentals(new Rental(new Movie(new RegularMovie("Film normal")), 5));
    }

    @Test
    public void statement()
    {
        StringBuilder resultBuilder = new StringBuilder("\n<h1>Rental Record for Sylvain</h1>\n<li>Film enfant</li><li>4.5</li>\n<li>Film normal</li><li>6.5</li>\n<div>Amount owed is 11.0</div>You earned 2 frequent renter points.");
        assertEquals(resultBuilder.toString(), this.c1.htmlStatement());
    }

}