package app.movie;

import app.movie.movieclass.ChildrenMovie;
import app.movie.movieclass.NewReleaseMovie;
import app.movie.movieclass.RegularMovie;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class MovieTest {
    private Movie movie;

    @Before
    public void setUp() throws Exception {
        this.movie = new Movie(new ChildrenMovie("Film enfant"));
    }
//
//    @After
//    public void tearDown() throws Exception {
//    }

    @Test
    public void getPrice() {

        //Film enfant
        assertEquals(4.5, movie.getPrice(5), .1);
        assertEquals(1.5, movie.getPrice(2), .1);

        //film regular
        movie.setMovieCLass(new RegularMovie("Regular"));
        assertEquals(6.5, movie.getPrice(5), .1);
        assertEquals(2, movie.getPrice(2), .1);

        //nouveau film
        movie.setMovieCLass(new NewReleaseMovie("New movie"));
        assertEquals(10, movie.getPrice(5), .1);
        assertEquals(4, movie.getPrice(2), .1);
    }

    @Test
    public void getFrequentPoint() {
        movie.setMovieCLass(new ChildrenMovie("Enfant"));
        assertEquals(1, movie.getFrequentPoint());

        movie.setMovieCLass(new RegularMovie("Regular"));
        assertEquals(1, movie.getFrequentPoint());

        movie.setMovieCLass(new NewReleaseMovie("New"));
        assertEquals(2, movie.getFrequentPoint());
    }

    @Test
    public void getTitle() {
        movie.setMovieCLass(new ChildrenMovie("Enfant"));
        assertEquals("Enfant", movie.getTitle());

        movie.setMovieCLass(new RegularMovie("Regular"));
        assertEquals("Regular", movie.getTitle());

        movie.setMovieCLass(new NewReleaseMovie("New"));
        assertEquals("New", movie.getTitle());
    }
}