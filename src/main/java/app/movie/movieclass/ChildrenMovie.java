package app.movie.movieclass;

import app.movie.movieclass.abstractmovie.AMovie;

public class ChildrenMovie extends AMovie {

    public ChildrenMovie(String title) {
        super(3, 1.5, 1.5, 1, title);
    }
}
