package app.movie.movieclass.abstractmovie;


public abstract class AMovie implements IMovieClass{
    private final int minDays;
    private final double basePrice;
    private final double supplementPrice;
    private int frequentPoint;
    private String title;

    protected AMovie(int minDays, double basePrice, double supplementPrice, int frequentPoint, String title) {
        this.minDays = minDays;
        this.basePrice = basePrice;
        this.supplementPrice = supplementPrice;
        this.frequentPoint = frequentPoint;
        this.title = title;
    }

    public double getPrice(Integer daysRented) {
        return (daysRented > minDays) ? basePrice + ((daysRented-minDays)*supplementPrice) : basePrice;
    }

    public int getFrequentPoint() {
        return frequentPoint;
    }

    public void setFrequentPoint(Integer frequentPoint) {
        this.frequentPoint = frequentPoint;
    }

    public String getTitle() {
        return this.title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
