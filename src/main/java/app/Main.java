package app;

import app.movie.Movie;
import app.movie.movieclass.ChildrenMovie;
import app.movie.movieclass.NewReleaseMovie;
import app.movie.movieclass.RegularMovie;

import java.util.logging.Level;
import java.util.logging.Logger;

public class Main {

    public static void main(String[] a){

        Movie m1 = new Movie(new ChildrenMovie("Film enfant"));

        Movie m2 = new Movie(new RegularMovie("Film normal"));

        Movie m3 = new Movie(new NewReleaseMovie("Nouveau film"));


        Customer c1 = new Customer("Sylvain");

        Rental r1 = new Rental(m1, 5);

        c1.addRentals(r1);

        c1.addRentals(new Rental(m2,10));

        c1.addRentals(new Rental(m3,5));

        Logger logger = Logger.getLogger("logger");

        String statConsole = c1.statement();
        logger.log(Level.INFO, statConsole);

        String statHtml = c1.htmlStatement();
        logger.log(Level.INFO, statHtml);


    }

}


