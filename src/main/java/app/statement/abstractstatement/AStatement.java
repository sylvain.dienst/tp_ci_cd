package app.statement.abstractstatement;

import app.Customer;
import app.Rental;

import java.util.List;

public abstract class AStatement implements IStatement{
    protected List<Rental> rentals;
    protected Customer customer;

    protected AStatement(List<Rental> rentals, Customer customer) {
        this.rentals = rentals;
        this.customer = customer;
    }

    public List<Rental> getRentals() {
        return rentals;
    }

    public void setRentals(List<Rental> rentals) {
        this.rentals = rentals;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }
}
