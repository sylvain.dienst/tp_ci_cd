package app.statement.decorating;

import app.Rental;
import app.statement.abstractstatement.AStatement;
import app.statement.decorator.StatementDecorator;

public class HtmlStatement extends StatementDecorator {

    public HtmlStatement(AStatement statement) {
        super(statement.getRentals(), statement.getCustomer());
        super.statement = statement;
    }

    public String statement() {
        statement.statement();

        StringBuilder resultBuilder = new StringBuilder("\n<h1>Rental Record for " + customer.getName() + "</h1>\n");
        for (Rental rent : this.rentals) {

            resultBuilder
                    .append("<li>")
                    .append(rent.getMovie().getTitle())
                    .append("</li>")
                    .append("<li>")
                    .append(rent.getMovie().getPrice(rent.getDaysRented()))
                    .append("</li>\n");
        }
        resultBuilder
                .append("<div>Amount owed is ")
                .append(customer.getTotalRentalPrice())
                .append("</div>");

        resultBuilder
                .append("You earned ")
                .append(customer.getTotalFrequentPoint())
                .append(" frequent renter points.");

        return resultBuilder.toString();
    }
}
